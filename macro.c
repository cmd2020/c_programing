
#include <stdio.h>
#define LINE 2
int main()
{


   printf("Current time and date: %s %s \n",__TIME__, __DATE__);

   printf("File: %s \n",__FILE__);

   printf("Line: %d \n",__LINE__);

   printf("ANSI standard C: %d \n",__STDC__);

}


/*
 * example:
 *
#ifdef MACRO
   // conditional codes
#endif
*/


/*
 * Here are some "predefined macros" in C programming:
 *
 * __DATE__  A string containing the current date
   __FILE__  A string containing the file name
   __LINE__  An integer representing the current line number
   __STDC__  If follows ANSI standard C, then the value is a nonzero integer
   __TIME__  A string containing the current date.
 *
 *
 */

