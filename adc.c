#include <built_in.h>
unsigned long adc_result1 = 0;

char text[500] = {0};

void main() {
 UART1_Init(256000);
 ADC_Set_Input_Channel(_ADC_CHANNEL_4);                     // Choose ADC channel
 ADC1_Init();                                               // Init
 Delay_ms(200);
 while(1) {

     adc_result1 = ADC1_Get_Sample(4);

     LongToStr(adc_result1, text);

     UART1_Write_Text(text);
     UART1_Write(13);
     UART1_Write(10);
     Delay_ms(5);

 }
}
