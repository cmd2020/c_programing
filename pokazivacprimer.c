/*
 Napisati funkciju koja istovremeno vraća dve vrednosti-
 količnik i ostatak dva data broja.
 Kao argumente funkcije koristiti pokazivače umesto običnih vrednosti*/



#include <stdio.h>

void Kolicnik_I_Ostatak(int x, int y, int* kolicnik, int* ostatak) {

	printf("Kolicnik se postavlja na adresu : %p\n", kolicnik);
	printf("Ostatak se postavlja na adresu : %p\n", ostatak);

	*kolicnik = x / y; *ostatak = x % y;
}

void main() {

	int kolicnik, ostatak; printf("Adresa promenljive kolicnik je: %p\n", &kolicnik);
	printf("Adresa promenljive ostatak je: %p\n", &ostatak);
	Kolicnik_I_Ostatak(5, 2, &kolicnik, &ostatak);
	printf("Vrednost kolicnika je: %d\n", kolicnik);
	printf("Vrednost ostatka je: %d\n", ostatak);
}
