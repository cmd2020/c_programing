#include <stdio.h>
#include <stdlib.h>
#define TV_MAX_CHANNELS 100

unsigned char tvStatus = 0;
unsigned int tvGetCurrentChannel = 1;

void tvTurnOn(){
    tvStatus = 1;
}

void tvTurnOff(){
    tvStatus = 0;
}

void tvChannelUp(){
    if(tvStatus == 0){
    return;
    }
    tvGetCurrentChannel++;

    if(tvGetCurrentChannel > TV_MAX_CHANNELS){
        tvGetCurrentChannel = 0;
    }

}

void tvChannelDown(){
    if(tvStatus == 0){
    return;
    }
    tvGetCurrentChannel--;

    if(tvGetCurrentChannel < 0){
        tvGetCurrentChannel = TV_MAX_CHANNELS;
    }
}

unsigned char tvGetStatus(){
      return tvStatus;
}

unsigned int tvGetCurrentChannel(){
    return tvGetCurrentChannel;
}

void tvPrintInformation(){
    printf( "TV is %s. Current channel is %d.\n",
             tvStatus == 1 ? "ON" : "OFF", 
             tvGetCurrentChannel); 
}
