/*
Example:
turn on / off tv,
change channel etc.
*/
#include <stdio.h>
#include <stdlib.h>
#include "tv.h"

void main(){

    tvPrintInformation();

    tvTurnOn();
    tvPrintInformation();


    tvTurnOff();
    tvPrintInformation();

    tvTurnOn();
    tvPrintInformation();

    tvChannelUp();
    tvPrintInformation();
    
    tvChannelUp();
    tvPrintInformation();

    tvTurnOff();
    tvPrintInformation();

    tvChannelUp();
    tvPrintInformation();

}
