
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

void cocktail_sort_shaker_sort(char *member, int no_times)
{
	register int i;
	int ready;
	char a;

	do
	{
		ready = 0;
		for(i = no_times - 1; i > 0; --i)
		{
			if(member[i - 1] > member[ i ])
			{
				a = member[i - 1];
				member[i - 1] = member[ i ];
				member[ i ] = a;
				ready = 1;
			}
		}
		for(i = 1; i < no_times; ++i)
		{
			if(member[i - 1] > member[ i ])
			{
				a = member[i - 1];
				member[i - 1] = member[ i ];
				member[ i ] = a;
				ready = 1;
			}
		}
	} while(ready);
}

int main(void)
{
	char my_string[255];
	printf("Please enter a string: ");
	gets(my_string);
	cocktail_sort_shaker_sort(my_string, strlen(my_string));
	printf("The result of the sort is %s\n", my_string);
	return 0;
}
