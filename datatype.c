#include <stdio.h>

void main()
{
    int godina;
    unsigned int razred;
    float cena;
    double plata;
    char simbol;

    printf("Unesite godinu rodjenja: ");
    scanf("%d", &godina);

    printf("Unesite koji razred je potrebno da ste zavrsili ");
    scanf("%u", &razred);

    printf("Unesite kolika je cena: ");
    scanf("%f", &cena);

    printf("Unesite platu zaposlenog: ");
    scanf("%lf", &plata);

    printf("Unesite specijalni simbol ");
    scanf(" %c", &simbol);

    printf("Godina rodjenja je %d. godine \n", godina);
    printf("Potrebno je da ste zavrsili %u. razred \n", razred);
    printf("Ccena je %.4f dinara \n", cena);
    printf("Plata zaposlenog je %.2f dinara \n", plata);
    printf("Specijalni simbol je %c \n", simbol);
}
